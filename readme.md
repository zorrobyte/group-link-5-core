Group Link 5 Core
==============
Version 1.0
Release date: 3/27/2014
 
Project state:
--------------
experimental
 
Credits/History
--------------

Toadlife - Operation Flashpoint Group Link The Original

KeyCat - Operation Flashpoint Group Link 2 The enhanced version

=\SNKMAN/= - Armed Assault Group Link 2 Plus with special thanks to ZoneKiller, KyleSarnik, Solus

=\SNKMAN/= - Armed Assault Group Link 3

snYpir, Zayfod, Toadlife, Igor Drukov, General Barron, TJ - Operation Flashpoint Mod ECP ( Enhanced Configuration Project )

thunderbird84, Harkonin - Operation Flashpoint Mod F.F.U.R

=\SNKMAN/= - Armed Assault 2 Group Link 4 Special FX Edition

The majority of the Arma community, repository and partial legal liability maintained by Zorrobyte - Armed Assault 3 Group Link 5 Core for Arma 3
 
Licence
--------------

Licence
This work is licensed under a Attribution-NonCommercial-ShareAlike 4.0 International license.
http://creativecommons.org/licenses/by-nc-sa/4.0/

You are free to:

Share — copy and redistribute the material in any medium or format
Adapt — remix, transform, and build upon the material
The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:

Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
NonCommercial — You may not use the material for commercial purposes.
ShareAlike — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.
No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
Notices:

You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation.
No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.
 
Project description
--------------

To maintain the core AI enhancements from Group Link for the Arma 3 engine and to further develop Group Link for the next iteration of Armed Assault.
 
Dependencies:
--------------

None known as of yet, GL4 suggested CBA however GL5 has been tested without CBA with no notable issues.
 
Documentation
--------------

Please see the Wiki at https://bitbucket.org/zorrobyte/group-link-5-core/wiki/Home 
 
Installation instructions
--------------

Copy the "@GL5" folder into your Arma 3 root directory.

Drag the "UserConfig" folder from "@GL5\UserConfig" in your Arma 3 root directory.
 
Additional Notes
--------------

Thank you to the Arma community for the support, without you GL5 wouldn't of been possible.
http://forums.bistudio.com/showthread.php?175184-Licensing-issues-with-porting-GL4-to-A3