// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0 Radio Chatter Config
// By =\SNKMAN/= modified for A3 by Zorrobyte
// ////////////////////////////////////////////////////////////////////////////

class CfgPatches 
{
	class GL5_Radio_Chatter
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.0;
		requiredAddons[] = {};
	};
};

#include "CfgSounds.hpp"