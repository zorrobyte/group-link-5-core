// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0 Chatter Config
// By =\SNKMAN/= modified for A3 by Zorrobyte
// ////////////////////////////////////////////////////////////////////////////

class cfgSounds
{
	// //////////////////////////////////////
	// West Radio Chatter Air ( Random )
	// //////////////////////////////////////

	class West_Air_v01a
	{
		name = "West_Air_v01a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v01.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v02a
	{
		name = "West_Air_v02a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v02.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v03a
	{
		name = "West_Air_v03a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v03.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v04a
	{
		name = "West_Air_v04a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v04.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v05a
	{
		name = "West_Air_v05a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v05.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v06a
	{
		name = "West_Air_v06a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v06.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v07a
	{
		name = "West_Air_v07a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v07.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v08a
	{
		name = "West_Air_v08a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v08.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v09a
	{
		name = "West_Air_v09a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v09.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v10a
	{
		name = "West_Air_v10a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v10.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v11a
	{
		name = "West_Air_v11a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v11.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v12a
	{
		name = "West_Air_v02a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v12.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v13a
	{
		name = "West_Air_v13a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v13.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v14a
	{
		name = "West_Air_v14a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v14.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v15a
	{
		name = "West_Air_v15a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v15.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v16a
	{
		name = "West_Air_v16a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v16.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v17a
	{
		name = "West_Air_v17a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v17.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v18a
	{
		name = "West_Air_v18a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v18.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v19a
	{
		name = "West_Air_v19a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v19.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v20a
	{
		name = "West_Air_v10a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v10.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v21a
	{
		name = "West_Air_v21a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v21.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v22a
	{
		name = "West_Air_v22a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v22.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v23a
	{
		name = "West_Air_v23a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v23.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v24a
	{
		name = "West_Air_v24a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v24.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v25a
	{
		name = "West_Air_v25a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v25.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v26a
	{
		name = "West_Air_v26a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v26.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v27a
	{
		name = "West_Air_v27a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v27.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v28a
	{
		name = "West_Air_v28a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v28.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v29a
	{
		name = "West_Air_v29a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v29.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v30a
	{
		name = "West_Air_v30a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v30.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v31a
	{
		name = "West_Air_v31a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v31.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v32a
	{
		name = "West_Air_v32a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v32.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v33a
	{
		name = "West_Air_v33a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v33.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v34a
	{
		name = "West_Air_v34a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v34.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v35a
	{
		name = "West_Air_v35a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v35.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v36a
	{
		name = "West_Air_v36a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v36.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v37a
	{
		name = "West_Air_v37a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v37.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v38a
	{
		name = "West_Air_v38a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v38.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v39a
	{
		name = "West_Air_v39a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v39.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v40a
	{
		name = "West_Air_v40a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v40.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v41a
	{
		name = "West_Air_v41a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v41.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v42a
	{
		name = "West_Air_v42a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v42.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v43a
	{
		name = "West_Air_v43a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v43.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v44a
	{
		name = "West_Air_v44a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v44.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v45a
	{
		name = "West_Air_v45a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v45.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v46a
	{
		name = "West_Air_v46a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v46.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v47a
	{
		name = "West_Air_v47a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v47.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v48a
	{
		name = "West_Air_v48a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v48.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v49a
	{
		name = "West_Air_v49a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v49.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v50a
	{
		name = "West_Air_v50a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v50.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v51a
	{
		name = "West_Air_v51a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v51.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v52a
	{
		name = "West_Air_v52a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v52.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v53a
	{
		name = "West_Air_v53a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v53.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v54a
	{
		name = "West_Air_v54a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v54.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v55a
	{
		name = "West_Air_v55a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v55.wss", db+15, 1};
		titles[] = {};
	};

	// //////////////////////////////////////
	// West Radio Chatter Air ( Conversation )
	// //////////////////////////////////////

	class West_Air_v56a
	{
		name = "West_Air_v56a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v56.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v57a
	{
		name = "West_Air_v57a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v57.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v58a
	{
		name = "West_Air_v58a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v58.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v59a
	{
		name = "West_Air_v59a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v59.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v60a
	{
		name = "West_Air_v60a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v60.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v61a
	{
		name = "West_Air_v61a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v61.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v62a
	{
		name = "West_Air_v62a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v62.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v63a
	{
		name = "West_Air_v63a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v63.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v64a
	{
		name = "West_Air_v64a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v64.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v65a
	{
		name = "West_Air_v65a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v65.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v66a
	{
		name = "West_Air_v66a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v66.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v67a
	{
		name = "West_Air_v67a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v67.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v68a
	{
		name = "West_Air_v68a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v68.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v69a
	{
		name = "West_Air_v69a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v69.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v70a
	{
		name = "West_Air_v70a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v70.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v71a
	{
		name = "West_Air_v71a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v71.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v72a
	{
		name = "West_Air_v72a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v72.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v73a
	{
		name = "West_Air_v73a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v73.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v74a
	{
		name = "West_Air_v74a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v74.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v75a
	{
		name = "West_Air_v75a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v75.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v76a
	{
		name = "West_Air_v76a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v76.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v77a
	{
		name = "West_Air_v77a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v77.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v78a
	{
		name = "West_Air_v78a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v78.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v79a
	{
		name = "West_Air_v79a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v79.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v80a
	{
		name = "West_Air_v80a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v80.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v81a
	{
		name = "West_Air_v81a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v81.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v82a
	{
		name = "West_Air_v82a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v82.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v83a
	{
		name = "West_Air_v83a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v83.wss", db+15, 1};
		titles[] = {};
	};

	// //////////////////////////////////////
	// West Radio Chatter Air ( Conversation )
	// //////////////////////////////////////

	class West_Air_v84a
	{
		name = "West_Air_v84a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v84.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v85a
	{
		name = "West_Air_v85a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v85.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v86a
	{
		name = "West_Air_v86a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v86.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v87a
	{
		name = "West_Air_v87a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v87.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v88a
	{
		name = "West_Air_v88a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v88.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v89a
	{
		name = "West_Air_v89a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v89.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v90a
	{
		name = "West_Air_v90a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v90.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v91a
	{
		name = "West_Air_v91a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v91.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v92a
	{
		name = "West_Air_v92a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v92.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v93a
	{
		name = "West_Air_v93a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v93.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v94a
	{
		name = "West_Air_v94a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v94.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v95a
	{
		name = "West_Air_v95a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v95.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_v96a
	{
		name = "West_Air_v96a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Chatter\West_Air_v96.wss", db+15, 1};
		titles[] = {};
	};

	// //////////////////////////////////////
	// West Radio Chatter Land ( Random )
	// //////////////////////////////////////

	class West_Land_v01a
	{
		name = "West_Land_v01a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v01.wss", db+15, 1};
		titles[] = {};
	};

	class West_Land_v02a
	{
		name = "West_Land_v02a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v02.wss", db+15, 1};
		titles[] = {};
	};

	class West_Land_v03a
	{
		name = "West_Land_v03a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v03.wss", db+15, 1};
		titles[] = {};
	};

	class West_Land_v04a
	{
		name = "West_Land_v04a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v04.wss", db+15, 1};
		titles[] = {};
	};

	class West_Land_v05a
	{
		name = "West_Land_v05a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v05.wss", db+15, 1};
		titles[] = {};
	};

	class West_Land_v06a
	{
		name = "West_Land_v06a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v06.wss", db+15, 1};
		titles[] = {};
	};

	class West_Land_v07a
	{
		name = "West_Land_v07a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v07.wss", db+15, 1};
		titles[] = {};
	};

	class West_Land_v08a
	{
		name = "West_Land_v08a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v08.wss", db+15, 1};
		titles[] = {};
	};

	class West_Land_v09a
	{
		name = "West_Land_v09a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v09.wss", db+15, 1};
		titles[] = {};
	};

	class West_Land_v10a
	{
		name = "West_Land_v10a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v10.wss", db+15, 1};
		titles[] = {};
	};

	class West_Land_v11a
	{
		name = "West_Land_v11a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v11.wss", db+15, 1};
		titles[] = {};
	};

	class West_Land_v12a
	{
		name = "West_Land_v12a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v12.wss", db+15, 1};
		titles[] = {};
	};

	class West_Land_v13a
	{
		name = "West_Land_v13a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v13.wss", db+15, 1};
		titles[] = {};
	};

	class West_Land_v14a
	{
		name = "West_Land_v14a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v14.wss", db+15, 1};
		titles[] = {};
	};

	// //////////////////////////////////////
	// East Radio Chatter Land ( Random )
	// //////////////////////////////////////

	class East_Land_v01a
	{
		name = "East_Land_v01a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v01.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v02a
	{
		name = "East_Land_v02a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v02.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v03a
	{
		name = "East_Land_v03a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v03.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v04a
	{
		name = "East_Land_v04a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v04.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v05a
	{
		name = "East_Land_v05a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v05.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v06a
	{
		name = "East_Land_v06a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v06.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v07a
	{
		name = "East_Land_v07a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v07.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v08a
	{
		name = "East_Land_v08a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v08.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v09a
	{
		name = "East_Land_v09a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v09.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v10a
	{
		name = "East_Land_v10a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v10.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v11a
	{
		name = "East_Land_v11a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v11.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v12a
	{
		name = "East_Land_v12a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v12.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v13a
	{
		name = "East_Land_v13a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v13.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v14a
	{
		name = "East_Land_v14a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v14.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v15a
	{
		name = "East_Land_v15a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v15.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v16a
	{
		name = "East_Land_v16a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v16.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v17a
	{
		name = "East_Land_v17a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v17.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v18a
	{
		name = "East_Land_v18a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v18.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v19a
	{
		name = "East_Land_v19a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v19.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v20a
	{
		name = "East_Land_v20a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v20.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v21a
	{
		name = "East_Land_v21a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v21.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v22a
	{
		name = "East_Land_v22a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v22.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v23a
	{
		name = "East_Land_v23a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v23.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v24a
	{
		name = "East_Land_v24a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v24.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v25a
	{
		name = "East_Land_v25a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v25.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v26a
	{
		name = "East_Land_v26a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v26.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v27a
	{
		name = "East_Land_v27a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v27.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v28a
	{
		name = "East_Land_v28a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v28.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v29a
	{
		name = "East_Land_v29a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v29.wss", db+15, 1};
		titles[] = {};
	};

	class East_Land_v30a
	{
		name = "East_Land_v30a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v30.wss", db+15, 1};
		titles[] = {};
	};

	// //////////////////////////////////////
	// West Radio Chatter Air and Land ( Random )
	// //////////////////////////////////////

	class West_Air_Land_v01a
	{
		name = "West_Air_Land_v01a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v01.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v02a
	{
		name = "West_Air_Land_v02a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v02.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v03a
	{
		name = "West_Air_Land_v03a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v03.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v04a
	{
		name = "West_Air_Land_v04a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v04.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v05a
	{
		name = "West_Air_Land_v05a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v05.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v06a
	{
		name = "West_Air_Land_v06a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v06.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v07a
	{
		name = "West_Air_Land_v07a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v07.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v08a
	{
		name = "West_Air_Land_v08a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v08.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v09a
	{
		name = "West_Air_Land_v09a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v09.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v10a
	{
		name = "West_Air_Land_v10a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v10.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v11a
	{
		name = "West_Air_Land_v11a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v11.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v12a
	{
		name = "West_Air_Land_v02a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v12.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v13a
	{
		name = "West_Air_Land_v13a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v13.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v14a
	{
		name = "West_Air_Land_v14a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v14.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v15a
	{
		name = "West_Air_Land_v15a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v15.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v16a
	{
		name = "West_Air_Land_v16a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v16.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v17a
	{
		name = "West_Air_Land_v17a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v17.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v18a
	{
		name = "West_Air_Land_v18a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v18.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v19a
	{
		name = "West_Air_Land_v19a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v19.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v20a
	{
		name = "West_Air_Land_v20a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v20.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v21a
	{
		name = "West_Air_Land_v21a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v21.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v22a
	{
		name = "West_Air_Land_v22a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v22.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v23a
	{
		name = "West_Air_Land_v23a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v23.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v24a
	{
		name = "West_Air_Land_v24a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v24.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v25a
	{
		name = "West_Air_Land_v25a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v25.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v26a
	{
		name = "West_Air_Land_v26a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v26.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v27a
	{
		name = "West_Air_Land_v27a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v27.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v28a
	{
		name = "West_Air_Land_v28a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v28.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v29a
	{
		name = "West_Air_Land_v29a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v29.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v30a
	{
		name = "West_Air_Land_v30a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v30.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v31a
	{
		name = "West_Air_Land_v31a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v31.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v32a
	{
		name = "West_Air_Land_v32a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v32.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v33a
	{
		name = "West_Air_Land_v33a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v33.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v34a
	{
		name = "West_Air_Land_v34a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v34.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v35a
	{
		name = "West_Air_Land_v35a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v35.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v36a
	{
		name = "West_Air_Land_v36a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v36.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v37a
	{
		name = "West_Air_Land_v37a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v37.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v38a
	{
		name = "West_Air_Land_v38a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v38.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v39a
	{
		name = "West_Air_Land_v39a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v39.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v40a
	{
		name = "West_Air_Land_v40a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v40.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v41a
	{
		name = "West_Air_Land_v41a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v41.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v42a
	{
		name = "West_Air_Land_v42a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v42.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v43a
	{
		name = "West_Air_Land_v43a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v43.wss", db+15, 1};
		titles[] = {};
	};

	class West_Air_Land_v44a
	{
		name = "West_Air_Land_v44a";
		sound[] = {"\GL5_Radio_Chatter\GL5_Air_Land_Chatter\West_Air_Land_v44.wss", db+15, 1};
		titles[] = {};
	};

	// //////////////////////////////////////
	// West Radio Chatter Land ( Random )
	// //////////////////////////////////////

	class West_Land_v01b
	{
		name = "West_Land_v01b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v01.wss", db+5, 1, 50};
		titles[] = {};
	};

	class West_Land_v02b
	{
		name = "West_Land_v02b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v02.wss", db+5, 1, 50};
		titles[] = {};
	};

	class West_Land_v03b
	{
		name = "West_Land_v03b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v03.wss", db+5, 1, 50};
		titles[] = {};
	};

	class West_Land_v04b
	{
		name = "West_Land_v04b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v04.wss", db+5, 1, 50};
		titles[] = {};
	};

	class West_Land_v05b
	{
		name = "West_Land_v05b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v05.wss", db+5, 1, 50};
		titles[] = {};
	};

	class West_Land_v06b
	{
		name = "West_Land_v06b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v06.wss", db+5, 1, 50};
		titles[] = {};
	};

	class West_Land_v07b
	{
		name = "West_Land_v07b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v07.wss", db+5, 1, 50};
		titles[] = {};
	};

	class West_Land_v08b
	{
		name = "West_Land_v08b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v08.wss", db+5, 1, 50};
		titles[] = {};
	};

	class West_Land_v09b
	{
		name = "West_Land_v09b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v09.wss", db+5, 1, 50};
		titles[] = {};
	};

	class West_Land_v10b
	{
		name = "West_Land_v10b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v10.wss", db+5, 1, 50};
		titles[] = {};
	};

	class West_Land_v11b
	{
		name = "West_Land_v11b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v11.wss", db+5, 1, 50};
		titles[] = {};
	};

	class West_Land_v12b
	{
		name = "West_Land_v12b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v12.wss", db+5, 1, 50};
		titles[] = {};
	};

	class West_Land_v13b
	{
		name = "West_Land_v13b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v13.wss", db+5, 1, 50};
		titles[] = {};
	};

	class West_Land_v14b
	{
		name = "West_Land_v14b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\West_Land_v14.wss", db+5, 1, 50};
		titles[] = {};
	};

	// //////////////////////////////////////
	// East Radio Chatter Land ( Random )
	// //////////////////////////////////////

	class East_Land_v01b
	{
		name = "East_Land_v01b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v01.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v02b
	{
		name = "East_Land_v02b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v02.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v03b
	{
		name = "East_Land_v03b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v03.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v04b
	{
		name = "East_Land_v04b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v04.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v05b
	{
		name = "East_Land_v05b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v05.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v06b
	{
		name = "East_Land_v06b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v06.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v07b
	{
		name = "East_Land_v07b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v07.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v08b
	{
		name = "East_Land_v08b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v08.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v09b
	{
		name = "East_Land_v09b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v09.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v10b
	{
		name = "East_Land_v10b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v10.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v11b
	{
		name = "East_Land_v11b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v11.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v12b
	{
		name = "East_Land_v12b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v12.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v13b
	{
		name = "East_Land_v13b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v13.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v14b
	{
		name = "East_Land_v14b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v14.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v15b
	{
		name = "East_Land_v15b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v15.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v16b
	{
		name = "East_Land_v16b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v16.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v17b
	{
		name = "East_Land_v17b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v17.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v18b
	{
		name = "East_Land_v18b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v18.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v19b
	{
		name = "East_Land_v19b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v19.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v20b
	{
		name = "East_Land_v20b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v20.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v21b
	{
		name = "East_Land_v21b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v21.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v22b
	{
		name = "East_Land_v22b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v22.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v23b
	{
		name = "East_Land_v23b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v23.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v24b
	{
		name = "East_Land_v24b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v24.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v25b
	{
		name = "East_Land_v25b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v25.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v26b
	{
		name = "East_Land_v26b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v26.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v27b
	{
		name = "East_Land_v27b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v27.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v28b
	{
		name = "East_Land_v28b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v28.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v29b
	{
		name = "East_Land_v29b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v29.wss", db+5, 1, 50};
		titles[] = {};
	};

	class East_Land_v30b
	{
		name = "East_Land_v30b";
		sound[] = {"\GL5_Radio_Chatter\GL5_Land_Chatter\East_Land_v30.wss", db+5, 1, 50};
		titles[] = {};
	};
};