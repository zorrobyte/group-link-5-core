// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Idea and Basic Script by Toadlife
// Modified and Enhanced by KeyCat
// Modified Optimized and Enhanced by =\SNKMAN/=
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c","_d","_e"];

waitUntil { (GL5_Initialize select 1) };

if (True) then
{
	GL5_System set [1, 0];

	_a = (GL5_Global select 8);

	_b = 200 + (random 100);

	_c = True;

	while { (_c) } do
	{
		_c = [True] call (GL5_System_F select 8);

		if (_c) then
		{
			if ( (GL5_System select 0) && { ( (GL5_System select 1) > _b) } && { (count (GL5_Reinforcement select 1) > 0) } ) then
			{
				GL5_System set [0, False];

				GL5_System set [1, 0];

				GL5_System set [2, objNull];

				sleep 5;

				[ (GL5_Reinforcement select 1) ] call (GL5_Remount_F select 0);

				GL5_Reinforcement set [1, [] ];

				call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_Groups.sqf");

				_b = 200 + (random 100);
			};

			_d = (GL5_Player select 0);

			_e = (GL5_Reinforcement select 0);

			[_e] call (GL5_System_F select 7);

			if ( [ (GL5_Reinforcement select 1) ] call (GL5_System_F select 9) ) then
			{
				[_d, _e] call (GL5_System_F select 0);

				sleep _a;
			}
			else
			{
				[_d, _e] call (GL5_System_F select 0);

				sleep (_a * 3);
			};
		}
		else
		{
			sleep 15;

			_c = [True] call (GL5_System_F select 8);
		};
	};
};