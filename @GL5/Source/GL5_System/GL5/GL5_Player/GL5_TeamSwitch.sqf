// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Team Switch
// Script by =\SNKMAN/= modified for A3 by Zorrobyte
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c","_d"];

_a = _this select 0;

while { (True) } do
{
	if (local _a) then
	{
		if ( { ( (alive _a) && { (alive _x) } && { (_a != _x) } ) } count (GL5_Player select 0) > 0) then
		{
			_a = player;

			[player] call compile preprocessFile (GL5_Path+"GL5\GL5_Player\GL5_Initialize.sqf");

			GL5_Player_PublicVariable = [player]; publicVariable "GL5_Player_PublicVariable";
		};
	};

	_b = (GL5_Player select 0);

	if (count (GL5_HC_Force_Move select 1) > 0) then
	{
		_b = _b - (GL5_HC_Force_Move select 1);
	};

	_c = 0;

	while { (_c < count _b) } do
	{
		_d = (_b select _c);

		if !( { ( (isPlayer _x) && (alive _x) ) } count (units _d) > 0) then
		{
			GL5_Player set [0, (GL5_Player select 0) - [_d] ];
		};

		_c = _c + 1;
	};

	sleep 30;
};