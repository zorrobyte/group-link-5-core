// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Spawn
// Script by =\SNKMAN/= modified for A3 by Zorrobyte
// ////////////////////////////////////////////////////////////////////////////
private ["_a"];

_a = _this select 0;

while { (True) } do
{
	sleep 1;

	if (alive player) exitWith
	{
		GL5_Player_PublicVariable = [player]; publicVariable "GL5_Player_PublicVariable";

		[player] call compile preprocessFile (GL5_Path+"GL5\GL5_Player\GL5_Initialize.sqf");

		if (player == leader player) then
		{
			if (GL5_Local select 1) then
			{
				[player] execVM (GL5_Path+"GL5\GL5_AddOns\GL5_Reloading\GL5_Reloading.sqf");
			};
		};
	};
};