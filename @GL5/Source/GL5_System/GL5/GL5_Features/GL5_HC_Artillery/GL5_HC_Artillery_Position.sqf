// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// High Command Artillery Position
// 
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c","_d","_e","_f","_g","_h"];

_a = _this select 0;
_b = _this select 1;

if (isMultiplayer) then
{
	GL5_HC_Artillery_Server_PublicVariable = [2, _a, _b]; publicVariable "GL5_HC_Artillery_Server_PublicVariable";
};

if (isServer) then
{
	GL5_HC_Artillery set [3, False];

	GL5_HC_Artillery set [4, False];

	if (isNil "GL5_HC_Artillery_Marker") then
	{
		GL5_HC_Artillery_Marker = "GL5_HC_Artillery_Marker";

		_c = GL5_HC_Artillery_Marker;

		createMarker [_c, _b];

		_c setMarkerShape "ICON"; _c setMarkerType "selector_selectedMission"; _c setMarkerSize [0.7, 0.7]; _c setMarkerText "<Artillery>"; _c setMarkerColor "ColorBlue";
	}
	else
	{
		GL5_HC_Artillery_Marker setMarkerPos _b;
	};

	_d = [ [" [playerSide, ""HQ""] sidechat""%1 artillery coordinates confirmed. Please stand by."" ", name _a],
	       [" [playerSide, ""HQ""] sidechat""%1 artillery target location confirmed. Please stand by."" ", name _a],
	       [" [playerSide, ""HQ""] sidechat""%1 artillery fire location confirmed. Please stand by."" ", name _a]

	] call GL5_Random_Select_F;

	if (local _a) then
	{
		call compile format _d;
	}
	else
	{
		GL5_HC_Artillery_Player_PublicVariable = [1, _a, _d]; publicVariable "GL5_HC_Artillery_Player_PublicVariable";
	};

	_e = [_a, _b] call (GL5_HC_Artillery_F select 0);

	if (_e select 0) exitWith
	{
		sleep 5 - (random 5);

		GL5_HC_Artillery set [3, True];

		GL5_HC_Artillery set [4, True];

		if (local _a) then
		{
			call compile format (_e select 1);
		}
		else
		{
			GL5_HC_Artillery_Player_PublicVariable = [1, _a, (_e select 1)]; publicVariable "GL5_HC_Artillery_Player_PublicVariable";
		};
	};

	if (alive _a) then
	{
		if (local _a) then
		{
			[3, _a] execVM (GL5_Path+"GL5\GL5_Features\GL5_HC_Artillery\GL5_HC_Artillery_Player.sqf");
		}
		else
		{
			GL5_HC_Artillery_Player_PublicVariable = [3, _a]; publicVariable "GL5_HC_Artillery_Player_PublicVariable";
		};

		sleep 10 - (random 5);

		if (GL5_HC_Artillery select 5) then
		{
			_d = [ [" [playerSide, ""HQ""] sidechat""%1 watch out! Artillery will start to fire within the next few seconds."" ", name _a],
			       [" [playerSide, ""HQ""] sidechat""%1 artillery ready for fire mission."" ", name _a],
			       [" [playerSide, ""HQ""] sidechat""%1 artillery support is on the way."" ", name _a],
			       [" [playerSide, ""HQ""] sidechat""%1 prepare for artillery fire mission."" ", name _a],
			       [" [playerSide, ""HQ""] sidechat""%1 artillery strike is on the way."" ", name _a]

			] call GL5_Random_Select_F;

			if (local _a) then
			{
				call compile format _d;
			}
			else
			{
				GL5_HC_Artillery_Player_PublicVariable = [1, _a, _d]; publicVariable "GL5_HC_Artillery_Player_PublicVariable";
			};

			_e = (GL5_HC_Artillery select 1);

			_f = 0;

			while { (_f < count _e) } do
			{
				_g = (_e select _f);

				_h = (vehicle leader _g);

				GL5_HC_Artillery set [6, (GL5_HC_Artillery select 6) + [_h] ];

				sleep 1 + (random 2);

				if ( (alive gunner _h) && (canFire _h) ) then
				{
					[_a, _h, _b] execVM (GL5_Path+"GL5\GL5_Features\GL5_HC_Artillery\GL5_HC_Artillery_Fire.sqf");
				};

				_f = _f + 1;
			};

			while { !(GL5_HC_Artillery select 3) } do
			{
				sleep 1;
			};

			if (GL5_HC_Artillery select 5) then
			{
				if (local _a) then
				{
					[5, _a] execVM (GL5_Path+"GL5\GL5_Features\GL5_HC_Artillery\GL5_HC_Artillery_Player.sqf");
				}
				else
				{
					GL5_HC_Artillery_Player_PublicVariable = [5, _a]; publicVariable "GL5_HC_Artillery_Player_PublicVariable";
				};

				_d = [ [" [playerSide, ""HQ""] sidechat""%1 artillery fire mission complete."" ", name _a],
				       [" [playerSide, ""HQ""] sidechat""%1 artillery support successful."" ", name _a]

				] call GL5_Random_Select_F;

				if (local _a) then
				{
					call compile format _d;
				}
				else
				{
					GL5_HC_Artillery_Player_PublicVariable = [1, _a, _d]; publicVariable "GL5_HC_Artillery_Player_PublicVariable";
				};
			};
		}
		else
		{
			GL5_HC_Artillery set [3, True];

			GL5_HC_Artillery set [7, time + (GL5_High_Command select 13) ];
		};
	};

	deleteMarker _c;

	GL5_HC_Artillery_Marker = nil;
};