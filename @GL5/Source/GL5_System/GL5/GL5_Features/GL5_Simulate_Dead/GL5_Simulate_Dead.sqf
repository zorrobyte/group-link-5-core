// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Dead Simulation
// Script by =\SNKMAN/= modified for A3 by Zorrobyte
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c"];

_a = _this select 0;
_b = _this select 2;

_a removeAction _b;

_a setCaptive True;

_a playAction "Unconscious";

sleep 1 + (random 1);

hintSilent "";

_c = _a addAction ["Stop Simulating Dead", (GL5_Path+"GL5\GL5_Features\GL5_Simulate_Dead\GL5_Simulate_Exit.sqf") ];

while { ( (alive _a) && (animationState _a == "Unconscious") ) } do  //AdthPpneMstpSrasWrflDnon_1
{
	sleep 5;

	if ( ((round(random 200)) < 1) && (captive _a) ) then
	{
		_a setCaptive False;

		hint "Warning! The Enemy recognized that you are simulating dead!";
	};
};

_a setCaptive False;

if (alive _a) then
{
	_a removeAction _c;

	sleep 50 + (random 100);

	GL5_Simulate set [0, (GL5_Simulate select 0) - [_a] ];
}
else
{
	_a removeAction _c;

	GL5_Simulate set [0, (GL5_Simulate select 0) - [_a] ];
};