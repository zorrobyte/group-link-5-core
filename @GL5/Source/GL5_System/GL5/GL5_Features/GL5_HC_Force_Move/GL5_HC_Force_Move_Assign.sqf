// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// High Command Force Move Assign
// 
// ////////////////////////////////////////////////////////////////////////////
private ["_a"];

_a = _this select 0;

while { (count (units _a) == 1) } do
{
	sleep 1;
};

if (alive _a) then
{
	call compile preprocessFile "\Ca\Modules\Functions\Misc\fn_commsMenuCreate.sqf";

	GL5_HC_Force_Move_Menu = [

		["H.C. Force Move", False],
		["Position ( Leader )", [2], "", -5, [ ["expression", "[player, (groupSelectedUnits player) ] execVM (GL5_Path+""GL5\GL5_Features\GL5_HC_Force_Move\GL5_HC_Force_Move.sqf"") "] ], "1", "1"],
		["Position ( Map - Click )", [3], "", -5, [ ["expression", "[player, (groupSelectedUnits player) ] execVM (GL5_Path+""GL5\GL5_Features\GL5_HC_Force_Move\GL5_HC_Force_Move_Position.sqf"") "] ], "1", "1"] 
	];

	[BIS_MENU_GroupCommunication, ["H.C. Force Move", [0], "#User:GL5_HC_Force_Move_Menu", -5, [ ["expression", ""] ], "1", "1"] ] call GL5_Array_Push_F;

	[BIS_MENU_GroupCommunication, ["", [], "", -1, [], "0", "0"] ] call GL5_Array_Push_F;
};
