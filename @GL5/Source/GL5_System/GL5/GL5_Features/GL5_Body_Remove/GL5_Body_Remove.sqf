// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Body Remove
// Script by =\SNKMAN/= modified for A3 by Zorrobyte
// ////////////////////////////////////////////////////////////////////////////
private ["_a"];

sleep 5 + (random 5);

while { (GL5_Global select 68) } do
{
	if (count (GL5_Killed select 0) > 0) then
	{
		_a = (GL5_Killed select 0);

		[_a] call (GL5_Body_Remove_F select 0);

		if (count (GL5_Killed select 0) > 100) then
		{
			[_a] spawn (GL5_Body_Remove_F select 4);
		};
	};

	sleep 240 + (random 240);
};