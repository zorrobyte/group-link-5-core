// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Spawn
// Script by =\SNKMAN/= modified for A3 by Zorrobyte
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b"];

_a = (_this select 0);

switch (_a) do
{
	case 1 :
	{
		publicVariable "GL5_Core";
	};

	case 2 :
	{
		waitUntil { (GL5_Initialize select 1) };

		if (typeName (GL5_Initialize select 0) == "Array") then
		{
			_b = (GL5_Initialize select 0);

			if (count (_b select 1) > 0) then
			{
				waitUntil { (GL5_Initialize select 2) };
			};

			publicVariable "GL5_Initialize";

			if (count (GL5_Recruit select 0) > 0) then
			{
				[] spawn
				{
					GL5_Recruit_Player_PublicVariable = [1, (GL5_Recruit select 0) ]; publicVariable "GL5_Recruit_Player_PublicVariable";
				};
			};
		};
	};
};