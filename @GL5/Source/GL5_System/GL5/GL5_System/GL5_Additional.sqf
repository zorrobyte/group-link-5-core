// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Additional
// By =\SNKMAN/= modified for A3 by Zorrobyte
// ////////////////////////////////////////////////////////////////////////////	
private ["_a","_b","_c"];

if (isServer) then
{
	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_Captive_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_Simulate_F.sqf");

	GL5_Recruit = [ [] ];

	_a = allUnits;

	_b = 0;

	while { (_b < count _a) } do
	{
		_c = (_a select _b);

		if (side _c in _this) then
		{
			if (GL5_Global select 66) then
			{
				GL5_Player set [2, (GL5_Player select 2) + [_c] ];
			};

			GL5_Recruit set [0, (GL5_Recruit select 0) + [_c] ];
		}
		else
		{
			[_c] spawn (GL5_Captive_F select 0);
		};

		_b = _b + 1;
	};

	if (count (GL5_Recruit select 0) > 0) then
	{
		[] spawn
		{
			waitUntil { (GL5_Initialize select 2) };

			if (isMultiplayer) then
			{
				GL5_Recruit_Player_PublicVariable = [1, (GL5_Recruit select 0) ]; publicVariable "GL5_Recruit_Player_PublicVariable";
			}
			else
			{
				[1, (GL5_Recruit select 0) ] execVM (GL5_Path+"GL5\GL5_Features\GL5_Recruit\GL5_Recruit_Player.sqf");
			};
		};
	};
}
else
{
	"GL5_Captive_Player_PublicVariable" addPublicVariableEventHandler { (_this select 1) execVM (GL5_Path+"GL5\GL5_Features\GL5_Captive\GL5_Captive_Player.sqf") };

	"GL5_Recruit_Player_PublicVariable" addPublicVariableEventHandler { (_this select 1) execVM (GL5_Path+"GL5\GL5_Features\GL5_Recruit\GL5_Recruit_Player.sqf") };
};

if (isDedicated) then
{
	"GL5_Captive_Server_PublicVariable" addPublicVariableEventHandler { (_this select 1) execVM (GL5_Path+"GL5\GL5_Features\GL5_Captive\GL5_Captive_Server.sqf") };

	"GL5_Recruit_Server_PublicVariable" addPublicVariableEventHandler { (_this select 1) execVM (GL5_Path+"GL5\GL5_Features\GL5_Recruit\GL5_Recruit_Server.sqf") };

	GL5_Initialize set [2, True];

	[2] execVM (GL5_Path+"GL5\GL5_System\GL5_Monitor.sqf");
}
else
{
	[] spawn
	{
		call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_Captive_F.sqf");

		call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_Simulate_F.sqf");

		GL5_Captive = [ [], [], [] ];

		GL5_Simulate = [ [] ];

		GL5_Initialize set [2, True];

		[2] execVM (GL5_Path+"GL5\GL5_System\GL5_Monitor.sqf");
	};
};