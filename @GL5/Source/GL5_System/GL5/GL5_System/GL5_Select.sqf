// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Select
// Script by =\SNKMAN/= modified for A3 by Zorrobyte
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c","_d","_e"];

_a = _this select 0;
_b = _this select 1;

if (isServer) then
{
	if (count _b > 0) then
	{
		_c = 0;

		while { (_c < count _a) } do
		{
			_d = (_a select _c);

			if (_d in _b) then
			{
				_a = _a - [_d];

				hint format ["Error: Friendly A.I. group(s) of the side %1 and leaded by A.I. was synchronized with the ""Group Link 5: System"" module. Please do only synchronize ENEMY A.I. groups leaded by A.I. and/or FRIENDLY A.I. groups leaded by a PLAYER with the ""Group Link 5: System"" module.", _d];
			};

			_c = _c + 1;
		};
	};

	if (count _a > 0) then
	{
		_a execVM (GL5_Path+"GL5\GL5_Database\GL5_Initialize.sqf");
	};

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_System_F\GL5_Difficult_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_System_F\GL5_Location_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_System_F\GL5_Rearm_F.sqf");

	GL5_Location = [ [] ];

	GL5_Rearm = [ [] ];

	_c = allUnits;

	_d = 0;

	while { (_d < count _c) } do
	{
		_e = (_c select _d);

		[_e] call (GL5_Difficult_F select 0);

		[_e] call (GL5_Difficult_F select 3);

		[_e] call (GL5_Location_F select 0);

		[_e] spawn (GL5_Rearm_F select 0);

		_d = _d + 1;
	};
};