// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Array Monitor
// Idea by Operation Flashpoint MOD E.C.P. ( Enhanced Configuration Project )
// Script by =\SNKMAN/= modified for A3 by Zorrobyte
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b"];

_a = _this select 0;

switch (_a) do
{
	case 1 :
	{
		while { (True) } do
		{
			if (count (GL5_Player select 0) > 0) then {	{if !(alive _x) then {GL5_Player set [0, (GL5_Player select 0) - [_x] ]; } }  forEach (GL5_Player select 0)	};

			sleep 30;

			if (count (GL5_Player select 1) > 0) then { {if ( ({ (alive _x) } count (units _x)) == 0) then {GL5_Player set [1, (GL5_Player select 1) - [_x] ]; } } forEach (GL5_Player select 1) };

			sleep 30;
		};
	};

	case 2 :
	{
		while { (True) } do
		{
			if (count (GL5_Captive select 0) > 0) then { {if ( !(alive _x) || (side _x != CIVILIAN) ) then {GL5_Captive set [0, (GL5_Captive select 0) - [_x] ]; } } forEach (GL5_Captive select 0) };

			sleep 30;

			if (count (GL5_Captive select 2) > 0) then { {if ( !(alive _x) || (side _x != CIVILIAN) ) then {GL5_Captive set [2, (GL5_Captive select 2) - [_x] ]; } } forEach (GL5_Captive select 2) };

			sleep 30;

			if (count (GL5_Simulate select 0) > 0) then { {if !(alive _x) then {GL5_Simulate set [0, (GL5_Simulate select 0) - [_x] ]; } } forEach (GL5_Simulate select 0) };

			sleep 30;

			if (isServer) then
			{
				if (count (GL5_Recruit select 0) > 0) then { {if !(alive _x) then {GL5_Recruit set [0, (GL5_Recruit select 0) - [_x] ]; } } forEach (GL5_Recruit select 0) };

				sleep 30;
			};
		};
	};

	case 3 :
	{
		while { (True) } do
		{
			if (count (GL5_Groups select 0) > 0) then {{if ( { (alive _x) } count (units _x) == 0) then {GL5_Groups set [0, (GL5_Groups select 0) - [_x] ] } } forEach (GL5_Groups select 0)};

			sleep 10;

			if (count (GL5_Defend select 0) > 0) then {{if ( { (alive _x) } count (units _x) == 0) then {GL5_Defend set [0, (GL5_Defend select 0) - [_x] ] } } forEach (GL5_Defend select 0)};

			sleep 10;

			if (count (GL5_Location select 0) > 0) then {{if ( { (alive _x) } count (units _x) == 0) then {GL5_Location set [0, (GL5_Location select 0) - [_x] ]; } } forEach (GL5_Location select 0)};

			sleep 10;

			if (count (GL5_Static select 0) > 0) then {{if ( { (alive _x) } count (units _x) == 0) then {GL5_Static set [0, (GL5_Static select 0) - [_x] ]; } } forEach (GL5_Static select 0)};

			sleep 10;

			if (count (GL5_Reinforcement select 0) > 0) then {{if ( { (alive _x) } count (units _x) == 0) then {GL5_Reinforcement set [0, (GL5_Reinforcement select 0) - [_x] ]; } } forEach (GL5_Reinforcement select 0)};

			sleep 10;

			if (count (GL5_Reinforcement select 1) > 0) then {{if ( { (alive _x) } count (units _x) == 0) then {GL5_Reinforcement set [1, (GL5_Reinforcement select 1) - [_x] ]; } } forEach (GL5_Reinforcement select 1)};

			sleep 10;

			if (count (GL5_House_Search select 1) > 0) then {{if ( { (alive _x) } count (units _x) == 0) then {GL5_House_Search set [1, (GL5_House_Search select 1) - [_x] ]; } } forEach (GL5_House_Search select 1)};

			sleep 10;

			if (count (GL5_Advancing select 0) > 0) then {{if ( { (alive _x) } count (units _x) == 0) then {GL5_Advancing set [0, (GL5_Advancing select 0) - [_x] ]; } } forEach (GL5_Advancing select 0)};

			sleep 10;

			if (count (GL5_Suppressed select 0) > 0) then {{if ( { (alive _x) } count (units _x) == 0) then {GL5_Suppressed set [0, (GL5_Suppressed select 0) - [_x] ]; } } forEach (GL5_Suppressed select 0)};

			sleep 10;

			if (count (GL5_Body_Detect select 0) > 0) then {{if ( { (alive _x) } count (units _x) == 0) then {GL5_Body_Detect set [0, (GL5_Body_Detect select 0) - [_x] ]; } } forEach (GL5_Body_Detect select 0)};

			sleep 10;

			if (count (GL5_Artillery select 0) > 0) then {{_b = (vehicle leader _x); if ( !(_b isKindOf "StaticMortar") || !(alive _b) || !(canFire _b) || !(alive gunner _b) ) then {GL5_Artillery set [0, (GL5_Artillery select 0) - [_x] ]; } } forEach (GL5_Artillery select 0)};

			sleep 10;

			if (count (GL5_Airstrike select 0) > 0) then {{_b = (vehicle leader _x); if ( !(_b isKindOf "Plane") || !(alive _b) || !(canFire _b) || !(alive driver _b) ) then {GL5_Airstrike set [0, (GL5_Airstrike select 0) - [_x] ]; } } forEach (GL5_Airstrike select 0)};

			sleep 10;

			if (count (GL5_Extraction select 0) > 0) then {{if ( { (alive _x) } count (units _x) == 0) then {GL5_Extraction set [0, (GL5_Extraction select 0) - [_x] ]; } } forEach (GL5_Extraction select 0)};

			sleep 10;

			if (count (GL5_Extraction select 1) > 0) then {{if ( { (alive _x) } count (units _x) == 0) then {GL5_Extraction set [1, (GL5_Extraction select 1) - [_x] ]; } } forEach (GL5_Extraction select 1)};

			sleep 10;

			if (count (GL5_Extraction select 2) > 0) then {{if ( { (alive _x) } count (units _x) == 0) then {GL5_Extraction set [2, (GL5_Extraction select 2) - [_x] ]; } } forEach (GL5_Extraction select 2)};

			sleep 10;

			if (count (GL5_Rearm select 0) > 0) then {{if (primaryWeapon _x == "") then {GL5_Rearm set [0, (GL5_Rearm select 0) - [_x] ]; } } forEach (GL5_Rearm select 0)};

			sleep 10;
		};
	};
};