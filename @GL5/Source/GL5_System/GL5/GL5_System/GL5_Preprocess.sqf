// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Preprocess
// By =\SNKMAN/= modified for A3 by Zorrobyte
// ////////////////////////////////////////////////////////////////////////////	
private ["_a","_b","_c","_d","_e"];

call compile preprocessFile (GL5_Path+"GL5\GL5_Database\GL5_Core.sqf");

call compile preprocessFile (GL5_Path+"GL5\GL5_Database\GL5_Local.sqf");

call compile preprocessFile (GL5_Path+"GL5\GL5_Database\GL5_Global.sqf");

call compile preprocessFile (GL5_Path+"GL5\GL5_Database\GL5_High_Command.sqf");

call compile preprocessFile (GL5_Path+"GL5\GL5_Database\GL5_Resource.sqf");

call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Extension_F.sqf");

call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_EH_F\GL5_EH_Fired_F.sqf");

call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_EH_F\GL5_EH_Hit_F.sqf");

call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_EH_F\GL5_EH_Killed_F.sqf");

call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_System_F\GL5_Icon_F.sqf");

GL5_Player_Id = objNull;

GL5_Group_Id = grpNull;

GL5_Player = [ [], [], [] ];

if (isServer) then
{
	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_System_F\GL5_Get_In_F.sqf");

	call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_System_F\GL5_Spawn_F.sqf");

	_a = allGroups;

	_b = 0;

	while { (_b < count _a) } do
	{
		_c = (_a select _b);

		_d = (vehicle leader _c);

		if (_d isKindOf "CaManBase") then
		{
			[_d] call (GL5_Get_In_F select 0);
		};

		_b = _b + 1;
	};
};

if (isDedicated) then
{
	GL5_Core set [4, False];

	"GL5_Player_PublicVariable" addPublicVariableEventHandler { (_this select 1) call compile preprocessFile (GL5_Path+"GL5\GL5_Player\GL5_Initialize.sqf") };

	"GL5_System_PublicVariable" addPublicVariableEventHandler { (_this select 1) execVM (GL5_Path+"GL5\GL5_System\GL5_Spawn.sqf") };

	[] spawn
	{
		waitUntil { (alive GL5_Player_Id) };

		[GL5_Player_Id] call compile preprocessFile (GL5_Path+"GL5\GL5_Player\GL5_Initialize.sqf");

		GL5_Initialize set [1, True];

		[1] execVM (GL5_Path+"GL5\GL5_System\GL5_Monitor.sqf");

		[GL5_Player_Id] execVM (GL5_Path+"GL5\GL5_Player\GL5_TeamSwitch.sqf");
	};
}
else
{
	[] spawn
	{
		private ["_e"];
		
		waitUntil { (player == player) };

		if (player == player) then
		{
			GL5_Player_Id = player; publicVariable "GL5_Player_Id";

			GL5_Group_ID = group player; publicVariable "GL5_Group_Id";

			[GL5_Player_Id] call compile preprocessFile (GL5_Path+"GL5\GL5_Player\GL5_Initialize.sqf");

			GL5_Player_PublicVariable = [player]; publicVariable "GL5_Player_PublicVariable";

			GL5_Initialize set [1, True];

			[1] execVM (GL5_Path+"GL5\GL5_System\GL5_Monitor.sqf");

			[GL5_Player_Id] execVM (GL5_Path+"GL5\GL5_Player\GL5_TeamSwitch.sqf");

			if (GL5_High_Command select 0) then
			{
				[player] execVM (GL5_Path+"GL5\GL5_System\GL5_HC_Reinforcement.sqf");
			};

			if (GL5_High_Command select 4) then
			{
				[player] execVM (GL5_Path+"GL5\GL5_System\GL5_HC_Helicopter.sqf");
			};

			if (GL5_High_Command select 8) then
			{
				[player] execVM (GL5_Path+"GL5\GL5_System\GL5_HC_Artillery.sqf");
			};

			if (GL5_Local select 86) then
			{
				if (player == leader player) then
				{
					GL5_HC_Force_Move = [ [], [], [] ];

					_e = (units player);

					_e = _e - [player];

					GL5_HC_Force_Move set [0, _e];

					call compile preprocessFile (GL5_Path+"GL5\GL5_Functions\GL5_Feature_F\GL5_HC_Force_Move_F.sqf");

					[player] execVM (GL5_Path+"GL5\GL5_Features\GL5_HC_Force_Move\GL5_HC_Force_Move_Assign.sqf");
				};
			};
		};
	};
};

call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_EH_Assign.sqf");