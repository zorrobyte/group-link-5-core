// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Synchronize
// Script by =\SNKMAN/= modified for A3 by Zorrobyte
// ////////////////////////////////////////////////////////////////////////////

if (count _this == 0) then
{
	sleep 1;

	if (GL5_Synchronize select 0) then
	{
		hint "Error: Please synchronize a enemy A.I. group with the ""Group Link 5: System"" module once to initialize the ""Group Link 5: Static"" module.";
	}
	else
	{
		if (GL5_Synchronize select 1) then
		{
			hint "Error: Please synchronize a enemy A.I. group with the ""Group Link 5: System"" module once to initialize the ""Group Link 5: Custom"" module.";
		};
	};
};