// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Time
// Script by =\SNKMAN/= modified for A3 by Zorrobyte
// ////////////////////////////////////////////////////////////////////////////
private ["_a","_b","_c","_d","_e","_f"];

_a = _this select 0;

if (_a isKindOf "Logic") then
{
	GL5_Time = [ False ];

	if (isServer) then
	{
		if (isNil "GL5_Random") then
		{
			call compile preprocessFile (GL5_Path+"GL5\GL5_Database\GL5_Random.sqf");
		};

		_b = (GL5_Random select 0);

		_c = (GL5_Random select 1);

		if (_b == 0) then
		{
			_d = (random 24);
		}
		else
		{
			_d = _b + (random _c);
		};

		skipTime _d;

		if (isMultiplayer) then
		{
			"GL5_Time_Server_PublicVariable" addPublicVariableEventHandler
			{
				_e = (_this select 1);

				_f = (_e select 0);

				if (_f != dayTime) then
				{
					GL5_Time set [0, date]; publicVariable "GL5_Time";
				};
			};
		};
	}
	else
	{
		waitUntil { (time > 0) };

		GL5_Time_Server_PublicVariable = [dayTime]; publicVariable "GL5_Time_Server_PublicVariable";

		waitUntil { (typeName (GL5_Time select 0) == Array) };

		_b = (GL5_Time select 0);

		setDate [ (_b select 0), (_b select 1), (_b select 2), (_b select 3), (_b select 4) ];
	};
};