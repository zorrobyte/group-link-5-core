// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// Public
// By =\SNKMAN/= modified for A3 by Zorrobyte
// ////////////////////////////////////////////////////////////////////////////	
private ["_a","_b"];

if (isServer) then
{
	_a = _this select 0;
	_b = _this select 1;

	GL5_Initialize set [0, [_a, _b] ]; publicVariable "GL5_Initialize";

	if (GL5_Core select 1) then
	{
		if (count _a > 0) then
		{
			_a execVM (GL5_Path+"GL5\GL5_System\GL5_Include.sqf");

			[_a, _b] call compile preProcessFile (GL5_Path+"GL5\GL5_System\GL5_Select.sqf");
		}
		else
		{
			_a execVM (GL5_Path+"GL5\GL5_System\GL5_Synchronize.sqf");
		};
	};

	if (GL5_Core select 2) then
	{
		if (count _b > 0) then
		{
			_b call compile preProcessFile (GL5_Path+"GL5\GL5_System\GL5_Additional.sqf");
		};
	};

	[_a, _b] call compile preprocessFile (GL5_Path+"GL5\GL5_System\GL5_HC_Reinforcement.sqf");
}
else
{
	if (typeName (GL5_Initialize select 0) != "Array") then
	{
		GL5_System_PublicVariable = [2]; publicVariable "GL5_System_PublicVariable";
	};

	[] spawn
	{
		private ["_a","_b"];
		
		waitUntil { (typeName (GL5_Initialize select 0) == "Array") };

		_a = (GL5_Initialize select 0);

		_b = (_a select 1);

		if (GL5_Core select 2) then
		{
			if (count _b > 0) then
			{
				_b call compile preProcessFile (GL5_Path+"GL5\GL5_System\GL5_Additional.sqf");
			};
		};
	};
};