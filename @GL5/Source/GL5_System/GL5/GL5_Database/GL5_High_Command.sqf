// ////////////////////////////////////////////////////////////////////////////
// GL5 v.1.0
// ////////////////////////////////////////////////////////////////////////////
// High Command Database
// Based on Operation Flashpoint Mod E.C.P. ( Enhanced Configuration Project )
// 
// ////////////////////////////////////////////////////////////////////////////

GL5_High_Command = [

	// #0
	True,
	10,
	50000,
	True,

	// #4
	True,
	3,
	50000,
	True,

	// #8
	True,
	2,
	"Sh_82mm_AMOS",
	3,
	50,
	300,
	True
];

if (GL5_Path == "\GL5_System\") then
{
	call compile preprocessFile "\UserConfig\GL5\GL5_High_Command.sqf";
};